module.exports = (sequelize, DataTypes) => {
  const Wine = sequelize.define('Wine', {
    name: DataTypes.STRING,
    label: DataTypes.STRING,
    etq_reco: DataTypes.STRING,
    width: DataTypes.INTEGER,
    targetId: DataTypes.STRING,
    title: DataTypes.STRING,
  }, {});

  Wine.associate = (models) => {
    Wine.hasMany(models.Note);
  };

  return Wine;
};
