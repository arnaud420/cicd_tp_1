module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define('Admin', {
    username: DataTypes.STRING,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [6, 20],
      },
    },
    isActive: DataTypes.BOOLEAN,
  }, {});

  Admin.prototype.toJSON = function () {
    const values = this.get();
    delete values.password;
    return values;
  };

  return Admin;
};
