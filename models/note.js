module.exports = (sequelize, DataTypes) => {
  const Note = sequelize.define('Note', {
    vintage: DataTypes.STRING,
    note: DataTypes.INTEGER,
    order: DataTypes.INTEGER,
    background: DataTypes.STRING,
  }, {});

  Note.associate = (models) => {
    Note.belongsTo(models.Wine);
  };

  return Note;
};
