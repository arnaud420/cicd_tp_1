module.exports = {
  "extends": "airbnb",
  "rules": {
    "indent": 2,
    "no-underscore-dangle": 0,
    "no-param-reassign": 0,
    "prefer-destructuring": 0,
    "func-names": 0,
    "no-shadow": 0,
    "no-console": 0,
    "no-tabs": 0
  }
};