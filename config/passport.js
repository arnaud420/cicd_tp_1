const passport = require('passport');
const passportJWT = require('passport-jwt');
const axios = require('axios');
const { JWT_SECRET } = require('./index');
const { Admin } = require('../models');

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

// Stratégie authentification administrateur
passport.use('admin-jwt', new JWTStrategy(
  {
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: JWT_SECRET,
    passReqToCallback: true,
  },
  async (req, jwtPayload, done) => {
    if (typeof jwtPayload.sub !== 'number' || jwtPayload.type_of_user !== 'admins') {
      return done(null, false);
    }
    try {
      // eslint-disable-next-line radix
      const admin = await Admin.findByPk(parseInt(jwtPayload.sub));
      if (!admin || admin === null) {
        return done(null, false);
      }
      axios.defaults.headers.common.Authorization = req.get('authorization');
      req.currentUser = admin;
      return done(null, admin);
    } catch (error) {
      return done(error, false);
    }
  },
));
