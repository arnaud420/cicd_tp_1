module.exports = {
  NODE_ENV: process.env.NODE_ENV || 'development',
  JWT_SECRET: process.env.JWT_SECRET,

  vuforia: {
    serverAccessKey: process.env.SERVER_ACCESS_KEY,
    serverSecretKey: process.env.SERVER_SECRET_KEY,
    clientAccessKey: process.env.CLIENT_ACCESS_KEY,
    clientSecretKey: process.env.CLIENT_SECRET_KEY,
  },

  development: {
    server: 'http://localhost:3003',
    client: 'http://localhost:3000',
    API_UPLOAD_CLOUD_URL: 'http://localhost:3004',
    postgres: {
      username: 'postgres',
      password: 'Jmd8Tlj3jkpg',
      database: 'simple_wine',
      host: 'localhost',
      dialect: 'postgres',
      port: 5432,
    },
  },
};
