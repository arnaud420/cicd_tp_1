const models = require('../models');

const { Wine, Note } = models;
let config = require('../config');

const { NODE_ENV } = config;
config = config[NODE_ENV];

module.exports = {
  getAll: async (req, res) => {
    try {
      const notes = await Note.findAll({
        include: [
          {
            model: Wine,
          },
        ],
      });
      res.status(200).json({
        success: true,
        data: notes,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },

  getOne: async (req, res) => {
    try {
      const note = await Note.findByPk(req.params.id, {
        include: [
          {
            model: Wine,
          },
        ],
      });

      res.status(200).json({
        success: true,
        data: note,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },

  create: async (req, res) => {
    try {
      const note = await Note.create(req.body);

      res.status(201).json({
        success: true,
        message: 'Note successfully created.',
        data: note,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },

  update: async (req, res) => {
    try {
      const note = await Note.update(req.body, {
        where: {
          id: req.params.id,
        },
      });

      res.status(200).json({
        success: true,
        message: 'Note successfully updated.',
        data: note,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },

  destroy: async (req, res) => {
    try {
      await Note.destroy({
        where: {
          id: req.params.id,
        },
      });
      res.status(200).json({
        success: true,
        message: 'Note successfully deleted.',
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },
};
