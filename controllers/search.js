const { Op } = require('sequelize');
const models = require('../models');

const { Wine } = models;

module.exports = {
  getWines: async (req, res) => {
    try {
      const wines = await Wine.findAll({
        where: {
          name: {
            [Op.iLike]: `%${req.params.search}%`,
          },
        },
      });
      res.status(200).json({ success: true, data: wines });
    } catch (e) {
      console.log('error', e);
      res.status(500).json({ success: false, error: e });
    }
  },
};
