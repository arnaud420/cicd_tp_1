const models = require('../models');

const { Wine, Note } = models;
let config = require('../config');

const { NODE_ENV } = config;
config = config[NODE_ENV];

module.exports = {
  getAll: async (req, res) => {
    try {
      const wines = await Wine.findAll({
        include: [
          {
            model: Note,
          },
        ],
        order: [
          ['id', 'DESC'],
        ],
        limit: 30,
      });
      res.status(200).json({
        success: true,
        data: wines,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },

  getOneFromSite: async (req, res) => {
    try {
      const wine = await Wine.findByPk(req.params.id, {
        include: [
          {
            model: Note,
          },
        ],
        order: [
          [models.Note, 'id', 'DESC'],
        ],
      });

      res.status(200).json({
        success: true,
        data: wine,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },

  create: async (req, res) => {
    try {
      const wine = await Wine.create(req.body);
      res.status(201).json({
        success: true,
        message: 'Wine successfully created.',
        data: wine,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },

  update: async (req, res) => {
    try {
      await Wine.update(req.body, {
        where: {
          id: req.params.id,
        },
      });

      const wine = await Wine.findByPk(req.params.id, {
        include: [
          {
            model: Note,
          },
        ],
        order: [
          [models.Note, 'id', 'DESC'],
        ],
      });
      res.status(200).json({
        success: true,
        message: 'Wine successfully updated.',
        data: wine,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },

  destroy: async (req, res) => {
    try {
      await Wine.destroy({
        where: {
          id: req.params.id,
        },
      });

      res.status(200).json({
        success: true,
        message: 'Wine successfully deleted.',
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        success: false,
        error,
      });
    }
  },
};
