FROM node:12-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN apk update && apk add --no-cache curl

RUN npm install

COPY . ./