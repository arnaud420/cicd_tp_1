const express = require('express');
const passport = require('passport');
require('../config/passport');

const router = express.Router();
const requireAdminAuth = passport.authenticate('admin-jwt', { session: false });

router.use('/wines', requireAdminAuth, require('./wines'));
router.use('/notes', requireAdminAuth, require('./notes'));

module.exports = router;
