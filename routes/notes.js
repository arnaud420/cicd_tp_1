const express = require('express');

const router = express.Router();
const notes = require('../controllers/notes');

router.get('/', notes.getAll);
router.post('/', notes.create);
router.get('/:id', notes.getOne);
router.patch('/:id', notes.update);
router.delete('/:id', notes.destroy);

module.exports = router;
