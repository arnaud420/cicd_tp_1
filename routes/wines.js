const express = require('express');
const passport = require('passport');

const router = express.Router();

const wines = require('../controllers/wines');

const requireAdminAuth = passport.authenticate('admin-jwt', { session: false });

router.get('/', wines.getAll);
router.post('/', requireAdminAuth, wines.create);
router.get('/:id', wines.getOneFromSite);
router.patch('/:id', requireAdminAuth, wines.update);
router.delete('/:id', requireAdminAuth, wines.destroy);

module.exports = router;
