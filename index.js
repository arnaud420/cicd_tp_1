const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');

const app = express();
const PORT = process.env.PORT || 3003;

// ------------------------------------------------ Middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// ---------------------------------------- API Protection with Helmet
app.use(helmet());

// ----------------------------- Allow cross-origin resource sharing
app.use(cors());

// ----------------------------------------- Log les requêtes (development)
app.use(morgan('dev'));

// ------------------------------------------------ Routes
app.use('/', require('./routes'));

// -------------------------------------------------- Start Server
app.listen(PORT, () => {
  console.log(`API is running on port ${PORT} ...`);
});
